


.. index::
   single: README

README
======


*Sphinx from Word* is a Word Template that converts correctly formatted Word Documents into Sphinx .rst files.



.. index::
   single: Version 1.3.1

Version 1.3.1
-------------


Updates for Sphinx 1.8 and some other tidy-ups



.. index::
   single: Version 1.3

Version 1.3
-----------


Images are now saved automatically into an images folder. The  |U8216r| Alt |U8217l|  text, will be preserved as Alternate Text.



.. index::
   single: Version 1.2

Version 1.2
-----------


added disappearing sidebar when screen is too narrow (and hamburger-button to show it)
|br| works with sphinx 1.7
|br| TOCTitle option now generates _template/globaltoc.html



.. index::
   single: Version 1.1

Version 1.1
-----------


added class:(style) support
|br| added options: CSS, themeOption, sidebar, sidebarWidth, navigation
|br| add support for super/subscripts and centre/right aligned paragraphs
|br| fixed links  |U8211n|  now work properly in tables and on images
|br| fixed WARNINGS on duplicate hyperlink text and formatting ending on a space
|br| fixed line breaks in tables



.. index::
   single: Version 1.0

Version 1.0
-----------


first release

.. |BR| raw:: html

   <BR/>


.. |U8217l| unicode:: U+02019
   :ltrim:
.. |U8216r| unicode:: U+02018
   :rtrim:
.. |U8211n| unicode:: U+02013
