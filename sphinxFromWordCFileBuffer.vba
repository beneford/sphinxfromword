' Sphinx from Word v1.3.1
' VBA module: sphinxFromWordInit
'
' Copyright (c) 2014-2018 Beneford Limited
' Source and binary code is distributed under the terms of the GNU General Public License.
'
' Sphinx from Word is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.
'
' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.
'
' You should have received a copy of the GNU General Public License
' along with this program.  If not, see <http://www.gnu.org/licenses/>.

Option Explicit

' Classes used by Sphinx From Word

Private fFile As Object         ' The file we're writing to
Private fFilename As String     ' The name of the file
Private buffer() As String      ' The buffer of lines waiting to be written
Private bufferPtr As Integer    ' The pointer to the current line in buffer
Private pending() As String     ' The pending lines delayed for later
Private pendingPtr As Integer   ' the pointer into the pending list
Private fLevel As Integer        ' The level the bufferPtr is current in
Private finally() As String     ' Lines to be output last
Private finallyPtr As Integer   ' the pointer into the finally list
Private marks() As String       ' This stores name=value
Private savePtr As Integer      ' The savePtr currently active
Private saveActive As Boolean   ' Flag to save savePtr is in use
Private fisTOCfile As Boolean   ' Flag that this is a TOCfile
Private fisNamedTOC As Boolean  ' Flag that this buffer is for a named TOC
Private fskipLines As Boolean   ' Flag that the next lines are to be skipped

Public Function NewFileBuffer(aFile As Object) As sphinxFromWordCFileBuffer
    Set NewBuffer = New sphinxFromWordCFileBuffer
    InitBuffer (aFile)
End Function

Public Sub InitBuffer(aFile As Object, aFilename As String)
    Set fFile = aFile
    fFilename = aFilename
    ReDim buffer(10)
    ReDim pending(10)
    ReDim finally(10)
    ReDim marks(1)
    bufferPtr = 1
    pendingPtr = 1
    level = -1
    isNamedTOC = False
    isTOCfile = False
    finallyPtr = 1
    savePtr = 0
    saveActive = False
End Sub

Public Sub addToBuffer(aLine As String)
    Dim i As Integer
    If Not skipLines Then
        addToBufferRaw (aLine)
    End If
    If aLine = "" And pendingPtr > 1 Then
        i = 1
        While i < pendingPtr
            addToBufferRaw pending(i)
            i = i + 1
        Wend
        pendingPtr = 1
    End If
End Sub

Private Sub addToBufferRaw(aLine As String)
    Dim i As Integer
    Dim p As Integer
    Dim sp As Integer
    If bufferPtr >= UBound(buffer) Then
        ReDim Preserve buffer(bufferPtr + 20)
    End If
    If saveActive Then
        For i = bufferPtr To savePtr + 1 Step -1
            buffer(i) = buffer(i - 1)
        Next
        buffer(savePtr) = aLine
        On Error Resume Next
        For i = 1 To UBound(marks)
            p = InStr(marks(i), "=")
            sp = CInt(Mid(marks(i), p + 1))
            If sp > savePtr Then
                marks(i) = Left(marks(i), p) + Trim(Str(sp + 1))
            End If
        Next i
        savePtr = savePtr + 1
    Else
        buffer(bufferPtr) = aLine
    End If
    bufferPtr = bufferPtr + 1
End Sub

Public Sub addPendingBlank(aLine As String)
    If pendingPtr >= UBound(pending) Then
        ReDim Preserve buffer(pendingPtr + 20)
    End If
    pending(pendingPtr) = aLine
    pendingPtr = pendingPtr + 1
End Sub

Public Sub addToFinally(aLine As String, Optional filterRepeat As Boolean = True)
    Dim j As Integer
    If filterRepeat Then
        j = finallyPtr - 1
        Do While j > 0
            If finally(j) = aLine Then ' we've already got this one
               Exit Do
            End If
            j = j - 1
        Loop
    Else
        j = 0
    End If
    If j = 0 Then
        If finallyPtr >= UBound(finally) Then
            ReDim Preserve finally(finallyPtr + 20)
        End If
        finally(finallyPtr) = aLine
        finallyPtr = finallyPtr + 1
    End If
End Sub

Public Sub setMark(name As String)
    Dim i As Integer
    Dim isSet As Boolean
    isSet = False
    If Len(name) > 0 Then
        For i = 1 To UBound(marks)
            If Left(marks(i), Len(name)) = name Then
                marks(i) = name & "=" & Trim(Str(bufferPtr))
                isSet = True
            End If
        Next i
        If Not isSet Then
            i = UBound(marks) + 1
            ReDim Preserve marks(i)
            marks(i) = name & "=" & Trim(Str(bufferPtr))
        End If
    Else
        savePtr = bufferPtr
    End If
End Sub

Public Sub activateMark(name As String)
    Dim i As Integer
    If Len(name) > 0 Then ' if no name given, then use savePtr as it is
        For i = 1 To UBound(marks)
            If Left(marks(i), Len(name)) = name Then
               On Error Resume Next
               savePtr = CInt(Mid(marks(i), Len(name) + 2))
               Exit For
            End If
        Next i
    End If
    saveActive = savePtr > 0
End Sub

Public Sub abortMark()
    saveActive = False
End Sub

Public Sub cancelMark()
    saveActive = False
    savePtr = 0
End Sub

Public Sub flushBuffer()
    Dim i As Integer
    For i = 1 To bufferPtr - 1
        fFile.writeline buffer(i)
    Next
    If finallyPtr > 1 Then fFile.writeline ""
    For i = 1 To finallyPtr - 1
        fFile.writeline finally(i)
    Next i
    bufferPtr = 1
End Sub

Property Let isTOCfile(TorF As Boolean)
    fisTOCfile = TorF
End Property

Property Get isTOCfile() As Boolean
    isTOCfile = fisTOCfile
End Property

Property Get bufferPointer() As Integer
    bufferPointer = bufferPtr
End Property

Property Let savePointer(sp As Integer)
    savePtr = sp
End Property

Property Get file() As Object
    Set file = fFile
End Property

Property Get isMarkActive() As Boolean
    Let isMarkActive = saveActive
End Property

Property Get filename() As String
    Let filename = fFilename
End Property

Property Get level() As Integer
    Let level = fLevel
End Property
Property Let level(v As Integer)
    fLevel = v
End Property

Property Let isNamedTOC(TorF As Boolean)
    fisNamedTOC = TorF
End Property
Property Get isNamedTOC() As Boolean
    isNamedTOC = fisNamedTOC
End Property
Property Let skipLines(TorF As Boolean)
    fskipLines = TorF
End Property
Property Get skipLines() As Boolean
    skipLines = fskipLines
End Property
